package webapp.com.om.map;

import org.apache.torque.TorqueException;

/**
 * This is a Torque Generated class that is used to load all database map 
 * information at once.  This is useful because Torque's default behaviour
 * is to do a "lazy" load of mapping information, e.g. loading it only
 * when it is needed.<p>
 *
 * @see org.apache.torque.map.DatabaseMap#initialize() DatabaseMap.initialize() 
 */
public class TurbineMapInit
{
    public static final void init()
        throws TorqueException
    {
        webapp.com.om.Table1Peer.getMapBuilder();
        webapp.com.om.TurbinePermissionPeer.getMapBuilder();
        webapp.com.om.TurbineRolePeer.getMapBuilder();
        webapp.com.om.TurbineGroupPeer.getMapBuilder();
        webapp.com.om.TurbineRolePermissionPeer.getMapBuilder();
        webapp.com.om.TurbineUserPeer.getMapBuilder();
        webapp.com.om.TurbineUserGroupRolePeer.getMapBuilder();
    }
}
